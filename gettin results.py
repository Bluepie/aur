__author__ = 'Gopinath (aka)BluePie'
#Email:Gopinath2nr@gmail.com (or) xzeropie@outlook.com
import urllib.request
from time import sleep
from base64 import b64decode as ph
from bs4 import BeautifulSoup as pg
from bs4 import BeautifulSoup as bs
import os
import argparse

parser = argparse.ArgumentParser(
	description='Tool to fetch results author @ Gopinath2nr@gmail.com (aka) BluePie')
parser.add_argument('-t', '--timeout', help="pass the timeout value", required=True)
parser.add_argument('-s', '--start', help="starting rollno", required=True)
parser.add_argument('-e', '--end', help="ending roll no", required=True)
args = parser.parse_args()


def writer(page):
	soup = bs(page, 'html.parser')
	f = open("results.txt", 'a')
	f.write("\n\n")
	for tag in soup.find_all('strong'):
		val=tag
		bufferread =[]
		for val in val.stripped_strings:
			bufferread.append(val)
		towrite ='\n'.join(bufferread)
		f.write('\n')
		f.write(towrite)

def writerwname(page,name):
	soup = bs(page, 'html.parser')
	f = open(name+'.txt', 'a')
	f.write("\n\n")
	for tag in soup.find_all('strong'):
		val=tag
		bufferread =[]
		for val in val.stripped_strings:
			bufferread.append(val)
		towrite ='\n'.join(bufferread)
		f.write('\n')
		f.write(towrite)

def getting(num,rs):
	placeholder = None
	reg ={'regno':num}
	reg =encode(reg)
	print('trying..{}'.format(num))
	while placeholder is None:
		try:
			response=placeholder=urllib.request.urlopen(rs,data=reg,timeout=int(args.timeout))
		except KeyboardInterrupt:
			raise
		except:
			print('No response..retrying..')
			pass
	while (response.getcode()) == "504":
		response=urllib.request.urlopen(rs,data=reg)
		print("retrying..")
	resp = response
	extracting(resp)

def encode(con):
	return (urllib.parse.urlencode(con)).encode('UTF-8')

def extracting(page):
	#p = pg(page, 'html.parser')
	if ch == "y":
		writerwname(page,rn)
		ch == "n"
	writer(page)
	

if __name__ == '__main__':
	print ('#'*80)
	print("Version:6.1")
	print('author:'+__author__)
	print('#'*80)
	print('')
	rs=ph("aHR0cDovL2F1Y29lLmFubmF1bml2LmVkdS9jZ2ktYmluL3Jlc3VsdC9jZ3JhZGUucGw=").decode('utf-8')
	ch = input("Do you want to fetch results for an individual: (y/n)")
	if ch == "y":
		print('')
		rn = input("Enter Your Rollno:")
		getting(rn,rs)
		print ('#'*80)
		print('')
		print("Wrote the result to file :{}".format(rn))
		print('')
		print ('#'*80)
		if input("Do you want to continue for the whole class:(y/n)") == 'n':
			quit()
	print ('#'*80)
	print("Initializing things for whole class")
	print ('#'*80)
	sleep(0.5)
	
	for r in range(int(args.start),int(args.end)+1):
		getting(r,rs)
'''
		if(r<10):
			rollnumber=c='start'
			rollnumber +=str(r)
			getting(rollnumber,rs)
			rollnumber=c
		else:
			rollnumber=c='7304141040'
			rollnumber +=str(r)
			getting(rollnumber,rs)
			rollnumber=c
'''
